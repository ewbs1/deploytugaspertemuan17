package mPermohonan

import (
	"database/sql"
)

type Permohonan struct {
	Id_permohonan  int64 `json:"Id,string"`
	Divisi         string
	NamaBarang     string
	Jumlah         int64 `json:"Jumlah,string"`
	Tgl_permintaan string
}

const dateFormat = `2006-01-02 15:04:05`

func SelectAll(db *sql.DB) (req []Permohonan, err error) {
	rows, err := db.Query(`SELECT * FROM permohonan ORDER BY id_permohonan DESC`)
	req = []Permohonan{}
	if err != nil {
		return req, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Permohonan{}
		err = rows.Scan(
			&s.Id_permohonan,
			&s.Divisi,
			&s.NamaBarang,
			&s.Jumlah,
			&s.Tgl_permintaan)
		if err != nil {
			return
		}
		req = append(req, s)
	}
	return
}

func Insert(db *sql.DB, m *Permohonan) (err error) {

	res, err := db.Exec(`INSERT INTO permohonan (divisi, namaBarang, jumlah, tgl_permintaan) VALUES (?,?,?,?)`,
		m.Divisi,
		m.NamaBarang,
		m.Jumlah,
		m.Tgl_permintaan)
	if err != nil {
		return err
	}
	m.Id_permohonan, err = res.LastInsertId()
	if err == nil {
		return
	}
	return err
}
