package main

import (
	"Permintaan/handler"
	"Permintaan/handler/hGuest"
)

const PORT = `:8181`

func main() {
	server := handler.InitServer(`views/`)
	server.Handle(`/guest/permohonan/list`, hGuest.PermohonanList)
	server.Handle(`/guest/permohonan/create`, hGuest.PermohonanCreate)
	server.Listen(PORT)
}
