package hGuest

import (
	"Permintaan/handler"
	"Permintaan/model/mPermohonan"
	"encoding/json"
	"net/http"
)

func PermohonanList(ctx *handler.Ctx) {
	Permohonan, err := mPermohonan.SelectAll(ctx.Db)
	if ctx.IsError(err) {
		return
	}
	ctx.End(Permohonan)
}

func PermohonanCreate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`guest/permohonan_create.html`)
		return
	}
	m := mPermohonan.Permohonan{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mPermohonan.Insert(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}

//endra wahyudi bangun saputra
