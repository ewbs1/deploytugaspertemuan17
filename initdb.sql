CREATE DATABASE req;
USE req;
CREATE USER `user02`@`localhost` IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON req.* TO `user02`@`localhost`;
FLUSH PRIVILEGES; 
-- mysql -u user02 -p user02

CREATE TABLE  permohonan (
  `id_permohonan` INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `divisi` varchar(20) NOT NULL,
  `namaBarang` varchar(5) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tgl_permintaan` varchar(20) NOT NULL
);
